#/usr/bin/bash


# First try termux way as pid is first and not second as on debian
pid=$(ps -ef |grep python|grep affiche_info_conf.py|sed -e's/  */ /g'|cut -f 1 -d ' ')
echo $pid|egrep '^[0-9 ]+$' >/dev/null
if [ $? -eq 0 ]; then
    kill $pid
    echo "Affiche info bus script detruit"
else
    pid=$(ps -ef |grep python|grep affiche_info_conf.py|grep -v grep | awk '{print $2}')
    echo $pid|egrep '^[0-9 ]+$' >/dev/null
    if [ $? -eq 0 ]; then
        kill $pid
    echo "Affiche info bus script detruit"
    fi
fi



# Detect working directory
lCheminTravail=$(cd `dirname $0` && pwd) >/dev/null
dossier=$lCheminTravail/../python

pyth=$lCheminTravail/../../venv/bin/python

if [ ! -e $pyth ] ; then
    echo "Python not found in $pyth"
    exit 1
fi

cd $dossier
$pyth affiche_info_conf.py WEB config_web_light.json

cd -  >/dev/null

exit 0
