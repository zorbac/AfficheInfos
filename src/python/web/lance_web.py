# -*- coding: latin-1 -*-
'''
@author: jingl3s

This is based on sample for qpython webapp
'''
# license
#
# Copyright jingl3s 
# This code is free software; you can redistribute it and/or modify it
# under the terms of the BSD license (see the file
# COPYING.txt included with the distribution).

import bottle

try:
    from web import services
    from web.server import MyWSGIRefServer
except Exception as e:
    print(e)


def lance_serveur(configuration, interaction, log_level: str="DEBUG"):
    services.set_interraction(interaction)
    services.set_configuration(configuration)

    app = bottle.default_app()

    try:
        ip_address = '0.0.0.0'
        server = MyWSGIRefServer(host=ip_address,  port=configuration['port'])

        reload_page = True
        debug_enabled = False
        quiet_mode = False

        # Settings for Operational usage or development purpose
        if log_level == "WARNING" or log_level == "ERROR":
            quiet_mode = True
            #server.quiet = True
            bottle.debug(False)
        else:
            # reloader permet de recharger des fichiers template modifie lors processus execution
            # recommende de le changer en prod
            reload_page = False
            bottle.debug(True)
            # For development purpose
            debug_enabled=True
#         from bottle import CherryPyServer

        app.run(server=server, reloader=reload_page,
                debug=debug_enabled, quiet=quiet_mode)
#        app.run(server='paste', reloader=reload_page, debug=debug_enabled, quiet=quiet_mode)
#         bottle.run(server='wsgiref', host=ip_address,  port=configuration['port'], reloader=reload_page, debug=debug_enabled, quiet=quiet_mode)
#         bottle.run(server='paste', host=ip_address,  port=configuration['port'], reloader=reload_page, debug=debug_enabled, quiet=quiet_mode)
#         bottle.run(server=CherryPyServer(
#             host=ip_address, port=configuration['port']), host=ip_address,  port=configuration['port'], reloader=reload_page)
#         bottle.run(server=PasteServer(host=ip_address, port=configuration['port']), host=ip_address,  port=configuration['port'], reloader=reload_page, debug=debug_enabled, quiet=True)

    except (Exception) as ex:
        print("Exception: %s" % repr(ex))
