# -*- coding: latin-1 -*-
"""
@author: jingl3s

"""
# license
#
# Copyright jingl3s 
# This code is free software; you can redistribute it and/or modify it
# under the terms of the BSD license (see the file
# COPYING.txt included with the distribution).

import datetime
import logging
from requests import Session
import socket
from unittest import mock
import unittest
from unittest.mock import patch


import sys

sys.path.append(".")
from donnees.horaire_stm import info_bus


class Test(unittest.TestCase):
    def setUp(self):
        pass

    def tearDown(self):
        pass

    def test_comparison_lot1(self):

        info_upper = info_bus.InformationBus("TestBus", "", 5, 5)
        current_dt1 = datetime.datetime.now()
        current_dt1 = current_dt1.replace(hour=8, minute=10)
        info_upper.add_extra_time("api", current_dt1, 5)

        info_lower = info_bus.InformationBus("TestBus", "", 4, 4)
        current_dt2 = datetime.datetime.now()
        current_dt2 = current_dt2.replace(hour=8, minute=9)
        info_lower.add_extra_time("api", current_dt2, 4)

        logging.basicConfig(level=logging.DEBUG)
        self._logger = logging.getLogger(self.__class__.__name__)
        self._logger.setLevel(logging.DEBUG)

        # self._logger.debug("Horaires trouvé v2: {}".format(sortie))
        self.assertLess(info_lower, info_upper, "Test full")

        info_lower = info_bus.InformationBus("TestBus", "", 4, 4)
        self.assertLess(info_lower, info_upper, "Test realtime")

        info_lower = info_bus.InformationBus("TestBus", "", 4)
        self.assertLess(info_lower, info_upper, "Test computed")

    def test_comparison_lot2(self):

        info_upper = info_bus.InformationBus("TestBus", "", 5, 5)
        current_dt1 = datetime.datetime.now()
        current_dt1 = current_dt1.replace(hour=8, minute=10)
        info_upper.add_extra_time("api", current_dt1, 5)

        info_lower = info_bus.InformationBus("TestBus", "", 4, 4)
        current_dt2 = datetime.datetime.now()
        current_dt2 = current_dt2.replace(hour=8, minute=9)
        info_lower.add_extra_time("api", current_dt2, 4)

        logging.basicConfig(level=logging.DEBUG)
        self._logger = logging.getLogger(self.__class__.__name__)
        self._logger.setLevel(logging.DEBUG)

        # self._logger.debug("Horaires trouvé v2: {}".format(sortie))
        self.assertLess(info_lower, info_upper, "Test full")

        info_upper = info_bus.InformationBus("TestBus", "", 5, 5)
        self.assertLess(info_lower, info_upper, "Test realtime")

        info_upper = info_bus.InformationBus("TestBus", "", 5)
        self.assertLess(info_lower, info_upper, "Test computed")

    def test_comparison_lot3(self):

        info_upper = info_bus.InformationBus("TestBus", "", 5, 5)

        info_lower = info_bus.InformationBus("TestBus", "", 4, 4)

        logging.basicConfig(level=logging.DEBUG)
        self._logger = logging.getLogger(self.__class__.__name__)
        self._logger.setLevel(logging.DEBUG)

        # self._logger.debug("Horaires trouvé v2: {}".format(sortie))
        self.assertLess(info_lower, info_upper, "Test full")

        info_upper = info_bus.InformationBus("TestBus", "", 5)
        self.assertLess(info_lower, info_upper, "Test computed")

    def test_comparison_lot4(self):

        info_upper = info_bus.InformationBus("TestBus", "", 5, 5)
        info_lower = info_bus.InformationBus("TestBus", "", 4)
        self.assertLess(info_lower, info_upper, "Test computed")


    def test_horaire_format(self):
        info_bus_in_progress = info_bus.InformationBus("TestBus", "00:03", 4, 4)
        current_dt2 = datetime.datetime.now()
        current_dt2 = current_dt2.replace(hour=23, minute=59)
        info_bus_in_progress.update_temps_attente(current_dt2)

        self.assertEqual("0:02", info_bus_in_progress.get_horaire_formate())
        
        current_dt2 = current_dt2 + datetime.timedelta(minutes=5)
        info_bus_in_progress.add_extra_time("api", current_dt2, 5)

        self.assertEqual("0:04", info_bus_in_progress.get_horaire_formate())

    def test_list_sort(self):
        """
        Case particulier ou le trie ne fonctionnait pas correctement
        Valeurs issue du cas de figure avec erreurs
        """
        dt_info = datetime.datetime(2020, 1, 21, 14, 22, 54)

        info1 = info_bus.InformationBus("51", "14:28", 5, 1)
        info1.add_extra_time("api", datetime.datetime(2020, 1, 21, 14, 29, 47), 6)
        info1.update_temps_attente(dt_info)

        info2 = info_bus.InformationBus("51", "14:42", 19, 12)
        info2.add_extra_time("api", datetime.datetime(2020, 1, 21, 14, 42, 25), 19)
        info2.update_temps_attente(dt_info)

        info3 = info_bus.InformationBus("51", "14:35", 12, 8)
        info3.add_extra_time("api", datetime.datetime(2020, 1, 21, 14, 37), 14)
        info3.update_temps_attente(dt_info)

        l_buses=list()
        l_buses.append(info2)
        l_buses.append(info1)
        l_buses.append(info3)
        
        l_expect=list()
        l_expect.append(info1)
        l_expect.append(info3)
        l_expect.append(info2)

        # ORganisation des resultats
        l_buses.sort()
      
        self.assertEqual(l_expect, l_buses)
  

if __name__ == "__main__":
    # import sys;sys.argv = ['', 'Test.testManageStm']
    unittest.main()
