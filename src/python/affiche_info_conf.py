# -*- coding: latin-1 -*-
'''
@author: jingl3s

'''
# license
#
# Copyright jingl3s 
# This code is free software; you can redistribute it and/or modify it
# under the terms of the BSD license (see the file
# COPYING.txt included with the distribution).

import os
import socket
import sys

from common.configuration_loader import ConfigurationLoader
from common.logger_config import LoggerConfig
from lanceur.lanceur import Lanceur
from tools import convert_config_path
from web import lance_web

# sys.path.append(os.path.dirname(__file__))


_logger = None


def configure(config_select=None, log_level: str="DEBUG"):
    '''
    :param config_select:

    :return: tuple avec en premier un logger configuré et en second la configuration json lues

    '''
    global _logger
    # LOGGER
    my_logger = LoggerConfig(os.path.join(
        os.path.dirname(__file__), "log"), "lanceur", log_level)
    _logger = my_logger.get_logger()

    _logger.info(f"LOGGER activated at level {log_level}")

    # CONFIG
    _config_json = None
    if config_select is not None:
        dossier_config = os.path.join(os.path.dirname(__file__),
                                      "configs")
        file = config_select
        gestion_config = ConfigurationLoader(
            dossier_config)
        _logger.info(f"Configuration file loaded {file}")
        gestion_config.set_configuration_file_name(file)
        _config_json = gestion_config.obtenir_configuration()

        _logger.debug(f"Configuration loaded {_config_json}")

    return _logger, _config_json, dossier_config


def update_secrets(config, config_dir):
    global _logger

    sys.path.append(config_dir)
    try:
        from configs import secrets
        for key in config.keys():
            for key_data in config[key]:
                if isinstance(config[key][key_data], dict) and "cle" in config[key][key_data]:
                    if hasattr(secrets, key_data):
                        config[key][key_data]["cle"] = getattr(
                            secrets, key_data)
                elif isinstance(config[key], dict) and "cle" == key_data:
                    if hasattr(secrets, key):
                        config[key]["cle"] = getattr(
                            secrets, key)
    finally:
        pass


def lance_affichage():
    global _logger

    if 2 <= len(sys.argv):
        mode_utilisateur = sys.argv[1]
    else:
        mode_utilisateur = 'WEB'

    config_file = "config_web.json"
    if 3 <= len(sys.argv):
        config_file = sys.argv[2]
    else:
        if mode_utilisateur == 'OLED':
            config_file = "config_web_hass.json"

    # Debug when developper computer
    hostname = socket.gethostname()
    if hostname in 'soniahugo-B85M-HD3':
        type_log = "DEBUG"
#         type_log = "WARNING"
#         type_log = "INFO"
    elif hostname in ['debian', 'n220']:
        type_log = "WARNING"
    else:
        type_log = "WARNING"
        # type_log = "DEBUG"

    _logger, config, config_dir = configure(config_file, type_log)

    update_secrets(config["DONNEES"], config_dir)

    dossier_travail = 'temp'
    if "DOSSIERS" in config:
        list_dossiers = convert_config_path.ConvertConfigPath.convert(os.path.dirname(
            __file__), config["DOSSIERS"])
        dossier_travail = ''
        for current_directory in list_dossiers:
            if os.path.exists(current_directory):
                dossier_travail = current_directory
                break

        # utilisation dernier current_directory et creation si non existant
        if dossier_travail == "":
            dossier_travail = list_dossiers[-1]
        if not os.path.exists(dossier_travail):
            os.mkdir(dossier_travail)

    _logger.debug(dossier_travail)

    if not "DONNEES" in config:
        raise RuntimeError('invalid param')
    if not "MODES" in config:
        raise RuntimeError('invalid param')

    if mode_utilisateur not in config['MODES']:
        mode_utilisateur = 'WEB'

    config_mode = config['MODES'][mode_utilisateur]

    if mode_utilisateur == 'WEB':
        configuration = config["MODES"]["WEB"]
        configuration['dossier'] = dossier_travail

        lanceur_donnees = Lanceur.affichage_mode(
            mode_utilisateur, dossier_travail, config["DONNEES"],  config_mode, p_eteindre_ecran=False)
        lance_web.lance_serveur(
            configuration, lanceur_donnees, log_level=type_log)
    else:
        Lanceur.affichage_mode(mode_utilisateur, dossier_travail,
                               config["DONNEES"], config_mode, p_eteindre_ecran=False)


if __name__ == '__main__':

    lance_affichage()
