# -*- coding: latin-1 -*-
'''
@author: jingl3s

'''
# license
#
# Copyright jingl3s 
# This code is free software; you can redistribute it and/or modify it
# under the terms of the BSD license (see the file
# COPYING.txt included with the distribution).

import copy
import datetime
import logging

from donnees.horaire_stm.stm_real_time import StmRealTime


class ManageSTMRealTime(object):
    '''
    Gestion des horaires STM en temps reel 

    '''
    TIME_BETWEEN_UPDATES = 2
    TIME_BETWEEN_FILL_DATA = 1

    def __init__(self):
        '''
        Constructor
        '''
        self._number_departure = 3
        self._logger = logging.getLogger(self.__class__.__name__)
#         self._logger.setLevel(logging.DEBUG)

        self._bus_rt_time = dict()
        self._list_bus_real_time = list()
        self._dt_prochain_real = datetime.datetime.now()
        self._current_index = 0
        self._last_update = datetime.datetime.now()
        self._bus_stop = 0

    def set_nombre_horaire(self, nombre_horaires):
        self._number_departure = nombre_horaires

    def set_bus_stop(self, bus_stop):
        '''

        :param bus_stop: string of bus stop number
        '''
        self._bus_stop = bus_stop

    def set_json_config(self, p_config: dict):
        if "ARRET" in p_config:
            self.set_bus_stop(p_config["ARRET"])

        if "BUS" in p_config:
            self._list_bus_real_time = list()
            for bus, bus_cfg in p_config["BUS"].items():

                if "REAL_TIME" in bus_cfg and len(bus_cfg["REAL_TIME"]) != 0:

                    if "DIR" in bus_cfg["REAL_TIME"] and bus_cfg["REAL_TIME"]["DIR"] != "":
                        bus_rt = StmRealTime()
                        bus_rt.set_ligne_bus(int(bus))
                        bus_rt.set_arret(int(self._bus_stop))
                        bus_rt.set_nombre_horaire(
                            bus_cfg["REAL_TIME"]["NUMBER"])
                        bus_rt.set_direction(bus_cfg["REAL_TIME"]["DIR"])
                        self._list_bus_real_time.append(bus_rt)

    def get_prochain_horaires(self, dt_courant):
        '''
        Retour du nombre d'horaire de bus

        @return: list des horaires de bus ordonnee sans information du bus
        '''

        # Verification si informations definies
        try:
            if len(self._list_bus_real_time) > 0:
                self._update_bus_real_time(dt_courant)
        except:
            self._logger.exception("Erreur STM RT")

        return copy.deepcopy(self._bus_rt_time)

    def _update_bus_real_time(self, p_datetime):
        '''
        Retrieve bus update from real time
        '''
        mise_a_jour = self._dt_prochain_real < p_datetime

        self._logger.debug("Heure courante: {}, attendu: {}".format(
            p_datetime, self._dt_prochain_real, ))

        if self._last_update is not None:
            # Update waiting time for the others
            if self._update_real_time_waiting(
                tps_delta=p_datetime - self._last_update):
                
                self._last_update = p_datetime

        if mise_a_jour:
            for index, bus_rt in enumerate(self._list_bus_real_time):
                # update only one item at a time to reduce server load
                if index == self._current_index:
                    list_results = bus_rt.get_buses()
                    # Break if error in at least one result
                    if len(list_results) > 0:
                        # Make deep copy in order to avoid sub class
                        # modification of values
                        self._bus_rt_time["{}".format(
                            bus_rt.get_bus())] = list_results[:]
                        
                        self._last_update = p_datetime

            self._current_index = (
                self._current_index + 1) % len(self._list_bus_real_time)

            # Update for next bus onlye on one execution
            next_bus = str(
                self._list_bus_real_time[self._current_index].get_bus())

            next_update = self.TIME_BETWEEN_UPDATES
            if not next_bus in self._bus_rt_time or 0 >= len(self._bus_rt_time[next_bus]):
                next_update = self.TIME_BETWEEN_FILL_DATA

            self._dt_prochain_real = p_datetime + datetime.timedelta(
                minutes=next_update)

            self._logger.debug("Stop: {}, Bus RT:{}".format(
                self._bus_stop, self._bus_rt_time))

    def _update_real_time_waiting(self, tps_delta):
        # Compute time between last update

        tps_decalage = int((tps_delta.seconds) / 60)

        is_updated = (0 != tps_decalage)

        if is_updated:

            last_index_to_delete = 0

            # Update time or remove oldest
            for _, horaire_rt in self._bus_rt_time.items():
                for idx_horaire, minute in enumerate(horaire_rt):
                    if minute > tps_decalage:
                        horaire_rt[idx_horaire] -= tps_decalage
                    elif minute < 0:
                        horaire_rt[idx_horaire] += tps_decalage
                        if horaire_rt[idx_horaire] >= 0:
                            horaire_rt[idx_horaire] = 0
                            last_index_to_delete = idx_horaire
                    elif minute > 0:
                        last_index_to_delete = idx_horaire

                # Delete any item on position to delete
                for _ in range(last_index_to_delete):
                    if 0 < len(horaire_rt):
                        del horaire_rt[0]


        return is_updated
