'''
@author: jingl3s

Copyright 2014 jingl3s 
This code is free software; you can redistribute it and/or modify it
under the terms of the BSD license (see the file
COPYING.txt included with the distribution).
'''
import requests
import json
import os

FILE_SAVE='config.json'

def save_json_data_to_file(url):
    try:
        response = requests.get(url)
        with open(FILE_SAVE, 'w') as f:
            json.dump(response.text, f)
    except Exception as e:
        raise Exception(e)
    return None

def get_json_data(url):
        
    with open(os.path.join(os.path.dirname(__file__),FILE_SAVE), 'r') as f:
        config = json.load(f)
    
    return config

if __name__ == '__main__':
    save_json_data_to_file()
    print(get_json_data())