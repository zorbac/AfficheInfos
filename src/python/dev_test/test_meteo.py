'''
@author: jingl3s

Copyright 2014 jingl3s 
This code is free software; you can redistribute it and/or modify it
under the terms of the BSD license (see the file
COPYING.txt included with the distribution).
'''
from meteo.meteo import ObtenirMeteo
from common.logger_config import LoggerConfig
import os

def do_stuff():
    path = os.path.abspath(os.path.dirname(__file__))
    filename_python = os.path.basename(__file__)

    if os.path.exists("/mnt/tmpfs/"):
        path_log = "/mnt/tmpfs/"
    else:
        path_log = path

    # Configuration des éléments du module
    logger_obj = LoggerConfig(
        path_log, os.path.splitext(os.path.split(filename_python)[1])[0])
    
    _logger = logger_obj.get_logger()
    meteo = ObtenirMeteo()
    meteo.set_weather_key("9c4fa08c1e177495")
    meteo.set_weather_location("pws:IQCMONTR29")
#     meteo_prevision = meteo.obtenir_meteo()
#     print(str(meteo_prevision))

    meteo._mise_a_jour_meteo_semaine()
    meteo_prevision = meteo.obtenir_meteo_semaine()
    print(str(meteo_prevision))

if __name__ == '__main__':
    do_stuff()