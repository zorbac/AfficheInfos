# -*- coding: latin-1 -*-
'''
@author: jingl3s

'''
# license
#
# Copyright jingl3s 
# This code is free software; you can redistribute it and/or modify it
# under the terms of the BSD license (see the file
# COPYING.txt included with the distribution).
from enum import Enum

class Afficheurs(Enum):
    PAGE_WEB = 1
    PAGE_OLED=2


class AfficheInfoFactory(object):
    '''
    Affichage de l'information sur l'ecran
    '''

    def __init__(self):
        '''
        Constructor
        '''
        pass
    @staticmethod
    def get_afficheur(p_type_afficheur):
        afficheur_demande = None
        if p_type_afficheur == Afficheurs.PAGE_WEB:
            from affichage_infos.page_web.page_web_jinja import PageWebLocale
            afficheur_demande = PageWebLocale()
        elif p_type_afficheur == Afficheurs.PAGE_OLED:
            from affichage_infos.ecran_oled.ecran_oled import EcranOLED
            afficheur_demande = EcranOLED()
        else:
            raise RuntimeError(
                "L'afficheur demand� n'existe pas : '{0}'".format(p_type_afficheur))
        return afficheur_demande
        
if __name__ == "__main__":
    print(hasattr(Afficheurs, 'PAGE_WEB2'))
