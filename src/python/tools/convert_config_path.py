# -*- coding: latin-1 -*-
'''
@author: jingl3s

'''
# license
#
# Copyright jingl3s 
# This code is free software; you can redistribute it and/or modify it
# under the terms of the BSD license (see the file
# COPYING.txt included with the distribution).

import os


class ConvertConfigPath():

    @staticmethod
    def convert(root_directory: str, dict_config: dict):
        """
        converti un dictionnaire de cle avec parametre en chemin connu
        retourne une liste de chemins concatene
        """
        list_path = list()
        
        list_expected_keys = ["absolu", "relatif", "utilisateur_relatif"]
        
        
        for key in list_expected_keys:
            if key in dict_config:
       
        # parcours de tous les chemins
            # definition du debut du chemin
                if "relatif" == key:
                    debut_path = root_directory
                elif "absolu" == key:
                    debut_path = ''
                elif "utilisateur_relatif" == key:
                    debut_path = os.path.expanduser("~")
                # traitement des dossiers
                for cur_path in dict_config[key]:
                    dossier_temp = os.path.join(debut_path, cur_path)
                    dossier_temp = os.path.realpath(dossier_temp)
                    list_path.append(dossier_temp)
        return list_path
    
    @staticmethod
    def get_valid_directory(list_dossiers_db:list) -> str:
        dossier_trouve = ""
        for current_directory in list_dossiers_db:
            if os.path.exists(current_directory):
                if len(os.listdir(current_directory)) > 1:
                    dossier_trouve = current_directory
                    break
        if "" == dossier_trouve:
            dossier_trouve = list_dossiers_db[0]
        return dossier_trouve


if __name__ == "__main__":
    data = dict()
    data["relatif"] = ["../../test_rel", "test_rel"]
    data["absolu"] = ["/home"]
    data["utilisateur_relatif"] = [".utilisateur/relatif"]
    dossiers = ConvertConfigPath.convert(os.path.dirname(__file__),data)
    print(dossiers)
