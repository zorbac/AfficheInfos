/*
@author: jingl3s

# license
#
# Copyright jingl3s 
# This code is free software; you can redistribute it and/or modify it
# under the terms of the BSD license (see the file
# COPYING.txt included with the distribution).
*/

var today= new Date();
var month = today.getMonth() + 1;
var day = today.getDate();
var year = today.getFullYear();
var hour = today.getHours();
var minutes = today.getMinutes();
var node = document.getElementById("date");
if (node) {
    node.innerHTML = "Rafraichissement page : " + day + "/" + month + "/" + year + " " + hour + ":" + minutes;
}

/* Affichage de l'heure */
/* http://stackoverflow.com/questions/18229022/how-to-show-current-time-in-javascript-in-the-format-hhmmss */
(function () {
    function checkTime(i) {
        return (i < 10) ? "0" + i : i;
    }

    function startTime() {
        var today = new Date(),
            h = checkTime(today.getHours()),
            m = checkTime(today.getMinutes()),
            s = checkTime(today.getSeconds());
        document.getElementById('time').innerHTML = h + ":" + m + ":" + s;
        t = setTimeout(function () {
            startTime()
        }, 500);
    }
    startTime();
})();


/*
Auto refresh page
*/


// Set a refresh based on existing connection to avoid refresh if not existing
// Source: https://stackoverflow.com/questions/22631869/how-to-refresh-page-using-javascript-if-connected
//setInterval(function() {
//    if (navigator.onLine) {
//        location.reload();
//    }
//}, 30000); /* milliseconds */


var window_visible_at_start = true;
var stateDelayReload = false;

// Source: https://stackoverflow.com/questions/19519535/detect-if-browser-tab-is-active-or-user-has-switched-away
// Source: https://stackoverflow.com/questions/1760250/how-to-tell-if-browser-tab-is-active
// "librairie" de gestion de la visibilité
//  var visible = vis(); // donne l'état courant
//  vis(function(){});   // définit un handler pour les changements de visibilité
var vis = (function(){
    var stateKey, eventKey, keys = {
        hidden: "visibilitychange",
        webkitHidden: "webkitvisibilitychange",
        mozHidden: "mozvisibilitychange",
        msHidden: "msvisibilitychange"
    };
    for (stateKey in keys) {
        //console.log(new Date, "Loop keys : " + stateKey);
        if (stateKey in document) {
            //console.log(new Date, "Loop keys read val: " + keys[stateKey]);    
            eventKey = keys[stateKey];
            break;
        }
    }
    return function(c) {
        if (c) {
            document.addEventListener(eventKey, c);
            //document.addEventListener("blur", c);
            document.addEventListener("focus", c);
            //console.log(new Date, "function c");
        }
        //console.log(new Date, "document visibility State : " + document["visibilityState"] + ",  return state :" + !document[stateKey] );
        return !document[stateKey];
    }
})();


vis(function(){
    // console.log(new Date, "main personnal function");
    
    visible_win = vis(); 
    
    if (visible_win) {
        // Refresh page if loose of focus and return - accelerate refresh to ensure up to date
        if (!window_visible_at_start)
        {
            //console.log(new Date, "title: Auto refresh -->");
            document.title = 'Auto refresh';
            location.reload();
        }
        
        // console.log(new Date, "title: Informations ref");
        document.title = 'Informations ref';
        
        if (! stateDelayReload)
        {
        // Reload the page after 30 seconds when focus is available
	        setInterval(function() {
    	        stateDelayReload = true;
        	    console.log(new Date, "Reload launch -->");
	            location.reload();
    	    }, 30000); /* milliseconds */
           // }, 6000); /* milliseconds */
        }
    }
    else {
        // console.log(new Date, "title: No refresh");
        document.title = 'No refresh';
        // Save window state as loose of focus
        window_visible_at_start = false;
        //TODO: Set here a variable to save this state as it will refresh page on next focus
    }

    // console.log(new Date, "Main script end window_visible_at_start : ", window_visible_at_start);
    //document.title = vis() ? 'Visible' : 'Not visible';
    //console.log(new Date, 'visible visible_win ?', visible_win);
});

// to set the initial state
//document.title = vis() ? 'Visible' : 'Not visible';

// Reffresh page if not available as it could be in displayed but the first part is not detected properly
if (vis() && document.title != 'Informations ref')
{
    if (! stateDelayReload)
    {

        setInterval(function() {
            stateDelayReload = true;
            console.log(new Date, "Reload launch main -->");
            location.reload();
        }, 60000); /* milliseconds */
    }
}

if(!vis())
{
        // console.log(new Date, "title: No refresh detected");
        document.title = 'No refresh detected';
        // Save window state as loose of focus
        window_visible_at_start = false;
} 



/*
Domoticz function call
*/


// Function to send URL get commands to Domoticz server
function send_domoticz(end_url)
{
    
    const url = "http://192.168.254.194:8080/json.htm?" + end_url;
    fetch(url).then(
            response => response.text() // .json(), etc.
            // same as function(response) {return response.text();}
    ).then(function(j) {
        // Yay, `j` is a JavaScript object
        // <!DOCTYPE ....
        console.log(j); 
        message = "Succes";
        //document.getElementById("div").innerHTML = "<BR><textarea rows='2' cols='2'>" + message + "</textarea>";
        console.log(message);
    }).catch(function(err) {
        message = "Erreur";
        //document.getElementById("div").innerHTML = "<BR><textarea rows='2' cols='2''>" + message + "</textarea>";
        console.log(message);
        // Error :(
        console.log(err);
});
    }
