<map version="1.0.1">
<!-- To view this file, download free mind mapping software FreeMind from http://freemind.sourceforge.net -->
<node CREATED="1478393709574" ID="ID_1348349416" MODIFIED="1478393716137" TEXT="Projet Afficheur">
<node CREATED="1478393744989" ID="ID_1087741153" MODIFIED="1478393748546" POSITION="right" TEXT="Id&#xe9;es">
<node CREATED="1478393800449" ID="ID_621752291" MODIFIED="1478393815183" TEXT="Afficher les prochains horaire de bus pour ne pas sortir au froids"/>
</node>
<node CREATED="1478393841104" ID="ID_1027292362" MODIFIED="1478393845350" POSITION="left" TEXT="Plan pr&#xe9;vu">
<node CREATED="1478393848586" ID="ID_614202996" MODIFIED="1478393859737" TEXT="Script Python qui g&#xe9;n&#xe8;re les informations"/>
<node CREATED="1478393867436" ID="ID_823304234" MODIFIED="1478393876528" TEXT="Plateforme RAspberry Pi"/>
<node CREATED="1478393879271" ID="ID_437567690" MODIFIED="1478393886384" TEXT="Utilisation d&apos;un afficheur LCD"/>
</node>
<node CREATED="1478394108647" ID="ID_398767213" MODIFIED="1478394111430" POSITION="left" TEXT="Plan suivi">
<node CREATED="1478393889741" ID="ID_1673428326" MODIFIED="1478394112947" TEXT="L&apos;afficheur LCD disponible n&apos;est pas compatible avec Rapsberry Pi">
<node CREATED="1478393961194" ID="ID_1891125871" MODIFIED="1478393980719" TEXT="Afficheur fonctionne en 5V donc avec Arduino &#xe7;a marche"/>
</node>
<node CREATED="1478394115344" ID="ID_1995694204" MODIFIED="1478394377304" TEXT="Commande d&apos;un &#xe9;cran OLED 1pouce"/>
<node CREATED="1478394429218" ID="ID_709716107" MODIFIED="1478394604949" TEXT="Utilisation du t&#xe9;l&#xe9;phone Nokia N8 en afficheur">
<node CREATED="1478394607805" ID="ID_216502268" MODIFIED="1478394625778" TEXT="Le t&#xe9;l&#xe9;phone fonctionne seulement en mode MTP pour partag&#xe9; la carte SD"/>
<node CREATED="1478394629551" ID="ID_1454452225" MODIFIED="1478394645166" TEXT="Le Rpi Debian ne reconnait pas correctement le nokia"/>
<node CREATED="1478394646927" ID="ID_949955548" MODIFIED="1478394727501" TEXT="Abandon id&#xe9;e car trop de travail"/>
</node>
<node CREATED="1478394729958" ID="ID_333586193" MODIFIED="1478394748766" TEXT="Utilisation du netbook pour premiers tests en attendant l&apos;afficheur">
<node CREATED="1478395273992" ID="ID_1002138105" MODIFIED="1478395276986" TEXT="Contrainte">
<node CREATED="1478395278122" ID="ID_1969261657" MODIFIED="1478395298204" TEXT="Utiliser &#xe9;cran de veille pour &#xe9;teindre l&apos;&#xe9;cran quand non utilis&#xe9;"/>
<node CREATED="1478395461660" ID="ID_1492721692" MODIFIED="1478395475926" TEXT="eteindre l&apos;&#xe9;cran automatiquement">
<node CREATED="1478395478653" LINK="https://www.kirsle.net/blog/entry/turn-off-monitor-from-linux-cli" MODIFIED="1478395478653" TEXT="https://www.kirsle.net/blog/entry/turn-off-monitor-from-linux-cli"/>
<node CREATED="1478395493718" ID="ID_1479050875" MODIFIED="1478395497270" TEXT="Autre id&#xe9;e">
<node CREATED="1478395497907" LINK="http://askubuntu.com/questions/62858/turn-off-monitor-using-command-line" MODIFIED="1478395497907" TEXT="askubuntu.com &gt; Questions &gt; 62858 &gt; Turn-off-monitor-using-command-line"/>
</node>
</node>
</node>
<node CREATED="1478395300497" ID="ID_1680351822" MODIFIED="1478395306519" TEXT="Distribution Linux">
<node CREATED="1478395307920" ID="ID_1723127042" MODIFIED="1478395312614" TEXT="Debian C++"/>
</node>
</node>
</node>
<node CREATED="1478394754028" ID="ID_1799670321" MODIFIED="1478394756680" POSITION="left" TEXT="Programmation">
<node CREATED="1478394882582" ID="ID_324294322" MODIFIED="1478394900094" TEXT="Recherche affichage page web statique ">
<node CREATED="1478394901900" ID="ID_675597544" MODIFIED="1478394997011" TEXT="Trouv&#xe9; information avec un outil qui nous donne une page web de base"/>
<node CREATED="1478394999783" ID="ID_1568225555" MODIFIED="1478395006541" TEXT="Recherche informations">
<node CREATED="1478395008002" ID="ID_224574881" MODIFIED="1478395018232" TEXT="Affichage de l&apos;heure en temps r&#xe9;el"/>
<node CREATED="1478395035504" ID="ID_121547455" MODIFIED="1478395044345" TEXT="Affichage d&apos;un tableau">
<node CREATED="1478395047434" ID="ID_1178331620" MODIFIED="1478395126763" TEXT="Utilisation d&apos;un affichage dynamique"/>
</node>
</node>
</node>
<node CREATED="1478395188149" ID="ID_1326433610" MODIFIED="1478395206064" TEXT="Affichage la page web sur le navigateur"/>
<node CREATED="1478395780458" ID="ID_1221969816" MODIFIED="1478395863493" TEXT="Utilisation d&apos;une lib Python3 pour les horaires bus">
<node CREATED="1478395865161" ID="ID_34202787" MODIFIED="1478395867045" TEXT="stmcli"/>
<node CREATED="1478395869486" ID="ID_432649406" MODIFIED="1478395880971" TEXT="D&#xe9;tournement du code du main pour faire mon traitement"/>
</node>
<node CREATED="1478560008500" ID="ID_1903415817" MODIFIED="1478560027492" TEXT="Utilisation d&apos;un dossier tmpfs pour la cr&#xe9;ation du fichier temporaire">
<node CREATED="1478560030320" ID="ID_1200461347" MODIFIED="1478560034621" TEXT="/mnt/tmpfs">
<node CREATED="1478560057582" MODIFIED="1478560057582" TEXT="tmpfs /mnt/tmpfs tmpfs defaults,noatime,nosuid,size=10m 0 0"/>
</node>
</node>
</node>
</node>
</map>
