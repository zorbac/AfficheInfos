Ce paquet me permet d'afficher les informations sur un ecran
Fonctions disponibles:
* Prochains horaire de bus STM
* Valeurs de capteur du serveur domoticz
* Prevision meteo jpurnee et semaine
* Activer des interrupteur sur domoticz

## Exemples visuels
<img src="images/capture_v8.png" alt="Developpement" width="800"> </img>

## Limites
Pas connues

## Dependances

* python >= 3.5
    * SQLite avec libsqlite3-dev pour peewee de stmcli



## Installations

pip3 install -r pip3-requires.txt

python3 src/python/affiche_info_conf.py <MODE>

<MODE> values
* WEB   demarre un server web qui écoute sur le port pat default
* STATIQUE  Creation d'un fichier HTML mis à jour toutes les minutes
* OLED      Affiche l'information sur un écran OLED branché

## Configuration

src/python/configs
* config_web.json configuration définie pour le server
* secrets.py    Fichier python contenant une variable par chaine 


## Historique versions

* 1 Premiere version

## Meta

Voir ``LICENSE`` pour plus d'information.

## Contribution

Fork du projet
```shell
git clone https://gitlab.com/jingl3s/AfficheInfos.git --recurse-submodules

Or if you have already cloned:

git submodule update --init --recursive

```


### Liens

#### Domoticz utilisation API
[domoticz wiki](https://www.domoticz.com/wiki/Domoticz_API/JSON_URL%27s)<br>
[domoticz API devices personnel](http://192.168.254.194:8080/json.htm?type=devices)<br>
[Tutoriel domoticz API](https://easydomoticz.com/manipuler-les-donnees-json-de-domoticz-en-shell/)<br>

### Python
#### Templating utilisé via Jinja2
[Jinja2 doc dev](http://jinja.pocoo.org/docs/dev/)<br>
[Jinja2 doc templates](http://jinja.pocoo.org/docs/dev/templates/)<br>
[Jinja2 doc templates avec parametres](http://jinja.pocoo.org/docs/dev/api/#jinja2.environment.TemplateStream.dump)<br>

#### Utilisation Datetime
[Stack overflow](http://stackoverflow.com/questions/23642676/python-set-datetime-hour-to-be-a-specific-time#23642695)

#### Serveur web
[Basic pour fichiers statique](http://stackoverflow.com/questions/5050851/best-lightweight-web-server-only-static-content-for-windows)

### CSS
[Table et CSS](https://css-tricks.com/complete-guide-table-element/)

### Weather Underground
[Example API](http://api.wunderground.com/api/API_KEY/hourly/lang:FR/q/pws:IQCMONTR29.json
[Prévisions](https://www.wunderground.com/weather/api/d/docs?d=data/forecast&MR=1)
[Utilisation API](https://www.wunderground.com/weather/api/d/docs?d=resources/code-samples&MR=1)

### Javascript
https://scotch.io/tutorials/how-to-use-the-javascript-fetch-api-to-get-data
https://davidwalsh.name/fetch

### Linux Debian configuration
* Autostart menu preference de OpenBox

### GTFS Feeds
* https://transitfeeds.com/p/societe-de-transport-de-montreal/39
* https://transit.land/feed-registry/operators/o-f25d-socitdetransportdemontral

## A Faire
* Mettre en place un fichier de configuration JSON avec toutes les données pour simplifier les entrée et les configurations voulues
  * Gestion plusieurs lignes de bus
  * Gestion mise en veille écran
  * Ajout future gestion du wifi par exemple
  * Lib python example possible: https://json-config.readthedocs.io/en/develop/contributing.html
* Remplacer utilisation de os.system par shell via [guide](http://stackoverflow.com/questions/204017/how-do-i-execute-a-program-from-python-os-system-fails-due-to-spaces-in-path)
* Remplacer la gestion de l'icon de Weather underground par téléchargement de l'icone au lieu de l'utilisation toutes les minutes
* Remplacer la gestion actuel par
  * Une methode commune comme update à toutes les classes qui génèrent une donnée
  * Une classe listener sur les données mises à jour lors du cycle d'update
  * Simplifie la lecture du code et la maintenance
* Ajouter envoi prévisions vers Domoticz
  * http://192.168.254.194:8080/json.htm?type=command&param=udevice&idx=39&nvalue=0&svalue=10;50;4
  * http://192.168.254.194:8080/json.htm?type=command&param=udevice&idx=40&nvalue=0&svalue=Averses%20de%20neiges
  * https://www.domoticz.com/wiki/Domoticz_API/JSON_URL%27s#Text_sensor
  
## Debug

```python
#         import sys;sys.path.append(r'/opt/eclipse/plugins/org.python.pydev_6.0.0.201709191431/pysrc')
#            import pydevd;pydevd.settrace('192.168.254.199')
        # import pydevd;pydevd.settrace('localhost')
        # import pdb; pdb.set_trace()
```

## Python depuis sources

Dependencies
* libsqlite3-dev


```shell
./configure --enable-optimizations --with-ensurepip=install --enable-loadable-sqlite-extensions
make -j 4
sudo make altinstall
```
